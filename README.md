# publizjr

![Logo](https://gitlab.com/zjr/publizjr/raw/master/stable/i/publizjr-logo-mar-2015-600x315.png "Publizjr Logo")


Publizjr - Creates web pages. Super fast, professional, accessible web site management and publication system. Requires PHP 5 or newer on the web server.

[Installation instructions](https://gitlab.com/zjr/publizjr/blob/master/stable/installation/publizjr-2-installation.txt)

[Quick start for content authors](https://gitlab.com/zjr/publizjr/wikis/Quickstart-for-content-authors)