<?php
declare(strict_types=1);
/** Basic Publizjr Generator Template.
 * Assembles article parts into html web pages.
 *
 * @link http://www.omegajunior.net/code/publizjr/
 * @use For the Publizjr PPS section on OmegaJunior.Net
 *
 * @author A.E.Veltstra <publizjr@omegajunior.net>
 * @copyright Copyright (c) A.E.Veltstra, 1993 - 2016
 * @version 20160729t1844
 */
ob_start('ob_gzhandler');

/* Configure: Uncomment the following to see any PHP errors in the output during testing */
/*
error_reporting( E_ALL );
ini_set( 'display_errors', true );
*/
header('content-type: text/html; charset=UTF-8');
header('cache-control: public, cache');
header('pragma: cache');
header('x-generated-by: Publizjr. http://www.omegajunior.net/code/publizjr/');

/* Configure: point to the correct file location. */
require_once dirname(__FILE__) . '/s/PublizjrPage.class.php';

/* Configure: supply the canonical web address that points to the directory where this template lives. HTML-attribute string. */
define('CANONICAL_SECTION_URI', 'http://www.omegajunior.net/code/publizjr/');
/* Configure: describe this web site section's contents. HTML-string. */
define('DEFAULT_PAGE_DESCRIPTION', '&Omega;Jr. Publizjr Creates Web Pages');
/* Configure: specify which child folder to interpret as the home page folder. */
define('DEFAULT_PAGE_ID', 'index');

/* Configure. Refer to the API documentation. */
$publizjrParams = array(
  'defaultPageID'                   => DEFAULT_PAGE_ID,
  'uriCanonicalSection'             => CANONICAL_SECTION_URI,
  'filePathToTranslations'          => './s/translations/en',
  'filenameCallToAction'            => 'callToAction',
  'filenameCallToActionCaption'     => 'callToActionCaption',
  'filenameDefaultVCardTranslation' => 'vCard',
  'strDefaultPageDescription'       => DEFAULT_PAGE_DESCRIPTION
);

$page = new PublizjrPage($publizjrParams);

if (!$page->exists) {
  header('status: 404', true);
  header('HTTP/1.0 404 Not Found', true);
}
header('last-modified: ' . $page->dateLastModified, true);

?>
<!DOCTYPE html>
<!-- Configure: default language setting and item type. -->
<html lang="en" itemscope itemtype="http://schema.org/Article">
<head>
  <meta charset="UTF-8">
  <?php
    if ($page->originalPageID !== DEFAULT_PAGE_ID) {
      echo "<title>$page->title - ", DEFAULT_PAGE_DESCRIPTION, "</title>";
    } else {
      echo "<title>$page->title</title>";
    }
  ?>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Configure: choose the stylesheet -->
  <link rel="stylesheet" media="all" href="c/publizjr.20160729t1825.css">
  <!-- Configure: if you have Facebook presence, provide this info. Otherwise, remove. Current values point to Publizjr, OmegaJunior.Net, and A.E.Veltstra on Facebook. -->
  <meta property="fb:app_id" content="138186404189">
  <meta property="fb:page_id" content="460288043991716">
  <meta property="fb:admins" content="821732366">
  <meta property="fb:profile_id" content="821732366">
  <!-- Configure: who is responsible for this article? -->
  <meta name="author" content="A.E.Veltstra, all rights reserved (c) 1993 - 2016">
  <!-- Configure: where can we find more information about this author? -->
  <link rel=author title="About A.E.Veltstra" href="http://linkedin.com/in/aeveltstra">
  <!-- Configure: where can Facebook users find more info about this author? -->
  <meta property="article:author" content="https://facebook.com/aeveltstra">
  <!-- Configure: where can Facebook users find more info about the organisation that published this content? -->
  <meta property="article:publisher" content="https://facebook.com/Publizjr">
  <!-- Configure: where can Twitter users find more info? -->
  <meta property="twitter:site" name="twitter:site" content="@publizjr">
  <!-- Configure: where can Google Plus users find more info? -->
  <link rel="publisher" href="https://plus.google.com/+OmegajuniorNet">
  <!-- Configure: where can RSS readers subscribe to updates? -->
  <link href="http://feeds.feedburner.com/webdev-OmegaJunior" type="application/rss+xml" rel="alternate"
        title="News Feed for &Omega;Jr. (Web) Software Development">
  <meta property="og:site_name" content='<?php

  echo DEFAULT_PAGE_DESCRIPTION, "'>
  <meta property='og:title' itemprop=name content='$page->title'>
  <meta property='og:url' content='$page->uriCanonical'>
  <link rel='canonical' itemprop=url href='$page->uriCanonical'>
  <meta name='twitter:url' content='$page->uriCanonical'>
  <meta name='twitter:title' content='$page->title'>
  <meta name='twitter:description' content='$page->description'>
  <meta name=description itemprop=description content='$page->description'>";
  if ($page->exists) {
    echo "  <meta property='pin:image' itemprop=image content='$page->pinImagePath'>
  <meta name='og:image' itemprop=image content='$page->ogImagePath'>
  <meta name='twitter:image:src' itemprop=image content='$page->twImagePath'>
  <meta property='og:type' content='", $page->readContentType(), "'>
  <meta name='twitter:card' content='", $page->readTwitterCardType(), "'>";
  }
  $page->addTranslation('favicons');
  $page->addExtendedCss();

  /* Configure: specify which image to show, if any */
  echo "</head>
<body ", $page->generateBodyClass(), ">
<header>
<h1><img src='i/publizjrlogo-256.png' align=left width=256>$page->title";
  if (!$page->exists || $page->originalPageID !== DEFAULT_PAGE_ID) {
    echo " - ", DEFAULT_PAGE_DESCRIPTION;
  }
  echo "</h1>
<p class=printOnly>This information lives on a webpage hosted at the following web address: '", CANONICAL_SECTION_URI, "'.</p>
</header>
<aside class=pathAndQ><p class=path>/ ";
  if ($page->originalPageID !== DEFAULT_PAGE_ID) {
    echo "<span itemscope itemtype='http://data-vocabulary.org/Breadcrumb'><a itemprop='url' href='./' title='", DEFAULT_PAGE_DESCRIPTION, "' rel=home><span itemprop='title'>Home</span></a></span> / ...";
  }
  echo '</p><section class=q>';
  $page->addSearchForm();
  echo "</section></aside>\n<article>";

  if (!$page->exists) {
    echo "<h2>$page->description</h2>\n";
    $page->addTranslation('ads-placeholder');
  } else {
    echo '<div class=intro><p>', $page->readIntro(), " </p></div>\n";
    $page->addWarningDeprecatedBrowser();
    $page->addTranslation('ads-placeholder');
    $page->addPageBody();
    $page->addExtendedHtml();
    $page->addCallToAction();
    /* Configure: add any additional links (inside LI elements) you wish to show on every page */
    $page->addPageLinks( /* '<li><a href="http://feeds.feedburner.com/webdev-OmegaJunior" type="application/rss+xml" rel="nofollow" title="Powered by Google Feedburner">RSS News Feed for &Omega;Jr. (Web) Software Development</a></li>' */);
  }

  ?>
  <h2 class="related">Connect to us on Social Media:</h2>
  <ul class="follow">
    <!-- Configure: supply the social media links you wish to share. -->
    <li><a rel=nofollow href="https://facebook.com/Publizjr">Publizjr @ Facebook</a></li>
    <li><a rel=nofollow href="https://twitter.com/publizjr">Publizjr @ Twitter</a></li>
    <li><a rel=nofollow href="https://plus.google.com/+OmegajuniorNet"> &Omega;Jr.Net @ Google Plus</a></li>
  </ul>
</article>
<footer>
<?php
  if ($page->exists) {
    $page->addSocialSharingButtons();
  }
  $page->addPageComments();
?>
<div class=addressInfo>
 <address>
  <?php $page->addVCard(); ?>
 </address>
 <hr>
 <!-- Configure: website footer. For instance, include a link to your privacy policy and your general disclaimer. -->
 <p>&copy; 2016 
 | <?php echo $page->readLastModifiedCaption(), ' ', $page->dateLastModified; ?>
 </p>
</div>
</footer>
<?php
  $page->addExtendedJs();
  $page->addTranslation('ads-script');
  $page->addTranslation('analytics');
?>
</body>
</html>
