<?php
/* 
A simple FAST parser to convert BBCode to HTML
Trade-in more restrictive grammar for speed and simplicty

Syntax Sample:
--------------
[img]http://elouai.com/images/star.gif[/img]
[url="http://elouai.com"]eLouai[/url]
[mail="webmaster@elouai.com"]Webmaster[/mail]
[size="25"]HUGE[/size]
[color="red"]RED[/color]
[b]bold[/b]
[i]italic[/i]
[u]underline[/u]
[list][*]item[*]item[*]item[/list]
[code]value="123";[/code]
[quote]John said yadda yadda yadda[/quote]

Usage:
------
include 'bb2html.php';
$htmltext = bb2html($bbtext);

(please do not remove credit)
author: Louai Munajim
website: http://elouai.com
date: 2004/Apr/18

Adapted by A.E.Veltstra for OmegaJunior.Net
2015-04-15
*/

function bb2html($text) {
  $bb = array('<', '>',
    '[*]', '[list]</li>', '[list]', '[/list]', 
    '[img]', '[/img]', 
    '[b][b][b]', '[/b][/b][/b]', '[b][b]', '[/b][/b]', '[b]', '[/b]',
    '[u]', '[/u]', 
    '[i]', '[/i]',
    '[dl]', '[dt]', '[/dt]', '[dd]', '[/dd]', '[/dl]', 
    '[noprint]', '[/noprint]',
    '[pre]', '[/pre]', 
    '[code]', '[/code]',
    '[printonly]', '[/printonly]',
    '[small]', '[/small]', 
    '[color="', '[color=&quot;', '[/color]',
    '[size="', '[size=&quot;', '[/size]',
    '[url="', '[url=&quot;', '[/url]', '&quot; target=&quot;', 
    '[mail="', '[mail=&quot;', '[/mail]',
    '[left]', '[/left]',
    '[center]', '[/center]',
    '[right]', '[/right]',
    '"]', '&quot;]');
  $html = array('&lt;', '&gt;',
    '</li><li>', '<ul>', '<ul>', '</li></ul>', 
    '<img alt="" src="', '">', 
    '<h2>', '</h2>', '<h3>', '</h3>', '<strong>', '</strong>',
    '<u>', '</u>', 
    '<em>', '</em>',
    '<dl>', '<dt>', '</dt>', '<dd>', '</dd>', '</dl>', 
    '<div class=noPrint>', '</div>',
    '<pre>', '</pre>', 
    '<code class="prettyprint">', '</code>',
    '<div class=printOnly>', '</div>',
    '<small>', '</small>', 
    '<span style="color:', '<span style="color:', '</span>',
    '<span style="font-size:', '<span style="font-size:', "</span>",
    '<a href="', '<a href="', '</a>', '" target="', 
    '<a href="mailto:', '<a href="mailto:', '</a>',
    '<div style="text-align: left">', '</div>',
    '<div style="text-align: center">', '</div>',
    '<div style="text-align: right">', '</div>',
    '">', '">');
  return nl2br(str_replace($bb, $html, $text));
}
?>
