<?php
/** zjrAcceptLanguage
 * The right way to detect visitor language preference.
 * Developed for the OmegaJunior.Net.
 *
 * @link https://omegajunior.globat.com/code/publizjr/
 * @author A.E.Veltstra <publizjr@protonmail.com>
 * @copyright Copyleft A.E.Veltstra, 2009.
 * @license Use and change to your heart's content.
 * @package Publizjr
 * @version 20150716t2224
 */
function zjrAcceptLanguage () {
  /* Configure: provide a default in case we don't find a preferred language.  */
  $strRequestedLanguageCode = 'en';
  /* Configure: the languages we claim to support. Enclosed in commas, like ',en,nl,', or ',fr,de,es,'. */
  $strCodesOfLanguagesWeSupport = strtolower(',en,nl,');

  $bUserBrowserProvidesLanguagePreferences = !empty( $_SERVER[ 'HTTP_ACCEPT_LANGUAGE' ] );
  if ($bUserBrowserProvidesLanguagePreferences) {
    $strUserProvidedLanguagePreferences = strtolower(htmlspecialchars('' . $_SERVER[ 'HTTP_ACCEPT_LANGUAGE' ]));
    $arrUserProvidedLanguagePreferences = explode(',', $strUserProvidedLanguagePreferences);
    $amount = count($arrUserProvidedLanguagePreferences);
    if ($amount) {
      for ($i = 0; $i < $amount; $i++) {
        $strSingleLanguagePreference = $arrUserProvidedLanguagePreferences[ $i ];
        $bContainsQualifier = ( strpos($strSingleLanguagePreference, ';') !== false );
        if ($bContainsQualifier) {
          $arrParts = explode(';', $strSingleLanguagePreference);
          $strSingleLanguagePreference = $arrParts[ 0 ];
        }
        $bContainsDialect = ( strpos($strSingleLanguagePreference, '-') !== false );
        if ($bContainsDialect) {
          $arrParts = explode('-', $strSingleLanguagePreference);
          $strSingleLanguagePreference = $arrParts[ 0 ];
        }
        $bWeSupportIt = ( strpos($strCodesOfLanguagesWeSupport, ',' . $strSingleLanguagePreference . ',') !== false );
        if ($bWeSupportIt) {
          $strRequestedLanguageCode = $strSingleLanguagePreference;
          break;
        }
      } /* end for */
    } /* end if($amount) */
  } /* end if($bUserBrowserProvidesLanguagePreferences) */
  return $strRequestedLanguageCode;
} /* end zjrAcceptLanguage() */
?>