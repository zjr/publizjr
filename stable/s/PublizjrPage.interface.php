<?php
/** Publizjr Web Page Generator Interface.
*
* Defines the basic api for a Publizjr Class.
* @link https://omegajunior.globat.com/code/publizjr/
* @author A.E.Veltstra <publizjr@protonmail.com>
* @copyright Copyright (c) 1993 - 2015, A.E.Veltstra for OmegaJunior.Net
* @version 20160729t1848
*
* @package Publizjr
*/
interface PublizjrPageInterface {
  function addCallToAction ();
  function addExtendedCss ( $deeperFolder = '' );
  function addExtendedHtml ( $deeperFolder = '' );
  function addExtendedJs ( $deeperFolder = '' );
  function addNavMenu ();
  function addPageBody ();
  function addPageComments ();
  function addPageLinks ( $extraLIs = '' );
  function addSearchForm ();
  function addSocialSharingButtons ();
  function addTranslation ( $which = '' );
  function addVCard ();
  function addWarningDeprecatedBrowser ();
  function doesPageExist ();
  function generateBodyClass ();
  function generateCanonicalURI ();
  function readContentType ();
  function readIntro ();
  function readLastModifiedCaption ();
  function readSchemaItemType ();
  function readTwitterCardType ();
  function readTranslation ( $which = '', $default = '' );
} 
