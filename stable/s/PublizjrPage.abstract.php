<?php
/** Abstract Publizjr Web Page Generator Class.
*
* Defines which methods and properties extending classes should implement.
* @link https://omegajunior.globat.com/code/publizjr/?id=publizjr-2-api-doc
* @author A.E.Veltstra <publizjr@protonmail.com>
* @copyright Copyright (c) 1993 - 2016, A.E.Veltstra for OmegaJunior.Net
* @version 20160729t1853
*
* @package Publizjr
*/
/**
* Implemented interface.
*
* Expects file PublizjrPage.interface.php to live in same directory.
*/
require_once dirname(__FILE__) . "/PublizjrPage.interface.php";
/**
* To convert BBCode to HTML.
*
* Expects file bb2html.php to live in same directory.
*/
require_once dirname(__FILE__) . "/bb2html.php";
/**
* Class PublizjrPageAbstract
*
* Sample usage:
* require_once dirname(__FILE__) . '/PublizjrPage.abstract.php';
* class PublizjrPage extends PublizjrPageAbstract {}
*
*/
abstract class PublizjrPageAbstract implements PublizjrPageInterface {

  protected $CANONICAL_SECTION_URI = '';
  protected $DEFAULT_PAGE_DESCRIPTION = '';
  protected $DEFAULT_PAGE_ID = "index";
  protected $FILE_PATH_TO_TRANSLATIONS = "./translations";
  protected $CALL_TO_ACTION_TRANSLATION_FILE = "callToAction";
  protected $CALL_TO_ACTION_CAPTION_TRANSLATION_FILE = "callToActionCaption";
  protected $DEFAULT_VCARD_TRANSLATION_FILE = 'vcard';

  protected $dateLastModified = '';
  protected $exists = false;
  protected $description = '';
  protected $id = "index";
  protected $langReq = "en";
  protected $ogImagePath = '';
  protected $originalPageID = "index"; //for smaller Multilanguage Class
  protected $pinImagePath = '';
  protected $title = '';
  protected $twImagePath = '';
  protected $uriCanonical = '';

  /**
  * Default property getter.
  *
  * Performs a basic check for existence of requested property and throws error E_USER_NOTICE if it does not. It is needed to allow read access to protected properties.
  *
  * @param $prop String, name of the requested property.
  * @return null or any, depending on the property type and whether or not the property exists.
  */
  public function __get( $prop ) {
    if ( property_exists( $this, $prop ) ) {
      return $this->$prop;
    }
    trigger_error( 'Undefined property requested on class ' . __CLASS__ . ': ' . $prop, E_USER_NOTICE );
    return null;
  }

  public function addCallToAction () {}
  public function addExtendedCss ( $deeperFolder = '' ) {}
  public function addExtendedHtml ( $deeperFolder = '' ) {}
  public function addExtendedJs ( $deeperFolder = '' ) {}
  public function addNavMenu () {}
  public function addPageBody () {}
  public function addPageComments () {}
  public function addPageLinks ( $extraLIs = '' ) {}
  public function addSearchForm () {}
  public function addSocialSharingButtons () {}
  public function addTranslation ( $which = '' ) {}
  public function addVCard () {}
  public function addWarningDeprecatedBrowser () {}
  public function doesPageExist () {}
  public function generateBodyClass () {}
  public function generateCanonicalURI () {}
  protected function lastExistingFileFromArray ( array $paths = array() ) {}
  protected function parseBBCode ( $strInput ) {}
  protected function parseNoCode ( $strInput ) {}
  protected function read404Title () {}
  protected function readBody () {}
  public function readContentType () {}
  protected function readDescription () {}
  protected function readFriendly404Message () {}
  public function readIntro () {}
  public function readLastModifiedCaption () {}
  protected function readMoreLinksCaption () {}
  protected function readOgImage () {}
  protected function readPageIDFromQuery () {}
  protected function readPart ( $strPartname, $bArray = false, $strDefault = '' ) {}
  protected function readPinImage () {}
  public function readSchemaItemType () {}
  protected function readTitle () {}
  public function readTranslation ( $which = '', $default = '' ) {}
  public function readTwitterCardType () {}
  protected function readTwImage () {
  }
} // end abstract class PublizjrPageAbstract
