<?php
/** Basic Publizjr Web Page Generator Class.
*
* Assembles article parts into html web pages.
* @link https://omegajunior.globat.com/code/publizjr/?id=publizjr-2-api-doc
* @author A.E.Veltstra <publizjr@protonmail.com>
* @copyright Copyright (c) 1993 - 2016, A.E.Veltstra for OmegaJunior.Net
* @version 20160729t1854
*
* @package Publizjr
*/
/**
* Extending the abstract class.
* Expects file PublizjrPage.abstract.php to live in same directory.
* @param __FILE__: makes file locator relative to current script.
*/
require_once dirname(__FILE__) . "/PublizjrPage.abstract.php";
/**
* Class PublizjrPage
*
* Sample usage:
* require_once dirname(__FILE__) . "/PublizjrPage.class.php";
* $page = new PublizjrPage( $params );
*
* @param array $params
* @see __construct()
*/
class PublizjrPage extends PublizjrPageAbstract {

  protected $CANONICAL_SECTION_URI = '';
  protected $DEFAULT_PAGE_DESCRIPTION = '';
  protected $DEFAULT_PAGE_ID = "index";
  protected $FILE_PATH_TO_TRANSLATIONS = "./translations";
  protected $CALL_TO_ACTION_TRANSLATION_FILE = "callToAction";
  protected $CALL_TO_ACTION_CAPTION_TRANSLATION_FILE = "callToActionCaption";
  protected $DEFAULT_VCARD_TRANSLATION_FILE = 'vcard';

  protected $dateLastModified = '';
  protected $exists = false;
  protected $description = '';
  protected $id = "index";
  protected $langReq = "en";
  protected $ogImagePath = '';
  protected $originalPageID = "index"; //allows for smaller Multilanguage Class
  protected $pinImagePath = '';
  protected $title = '';
  protected $twImagePath = '';
  protected $uriCanonical = '';

  /**
  * Default property getter.
  *
  * Performs a basic check for existence of requested property and throws error E_USER_NOTICE if it does not. It is needed to allow public read access to protected properties.
  *
  * @param $prop String, Name of the requested property.
  * @return null or any, depending on whether or not the property exists and its data type.
  */
  public function __get( $prop ) {
    if ( property_exists( $this, $prop ) ) {
      return $this->$prop;
    }
    trigger_error( 'Undefined property requested on class ' . __CLASS__ . ': ' . $prop, E_USER_NOTICE );
    return null;
  }

  /**
  * PublizjrPage Class Constructor
  *
  * Sample usage:
  *
  * define( 'DEFAULT_PAGE_ID', 'index' );
  * define( 'DEFAULT_PAGE_DESCRIPTION', '&Omega;Jr. Publizjr - Creates Web Pages' );
  * define( 'CANONICAL_SECTION_URI', 'https://omegajunior.globat.com/code/publizjr/' );
  *
  * $params = array(
  *  'defaultPageID'                   => DEFAULT_PAGE_ID,
  *  'uriCanonicalSection'             => CANONICAL_SECTION_URI,
  *  'filePathToTranslations'          => './s/translations',
  *  'filenameCallToAction'            => 'callToAction',
  *  'filenameCallToActionCaption'     => 'callToActionCaption',
  *  'filenameDefaultVCardTranslation' => 'vCard',
  *  'strDefaultPageDescription'       => DEFAULT_PAGE_DESCRIPTION
  * );
  *
  * $page = new PublizjrPage( $params );
  *
  * @param array $params
  */
  public function __construct ( array $params = array() ) {

    if ( !empty( $params ) ) {
      if ( !empty( $params[ 'defaultPageID' ] ) ) {
        $this->DEFAULT_PAGE_ID = (string) $params[ 'defaultPageID' ];
      }
      if ( !empty( $params[ 'uriCanonicalSection' ] ) ) {
        $this->CANONICAL_SECTION_URI = (string) $params[ 'uriCanonicalSection' ];
      }
      if ( !empty( $params[ 'filePathToTranslations' ] ) ) {
        $this->FILE_PATH_TO_TRANSLATIONS = (string) $params[ 'filePathToTranslations' ];
      }
      if ( !empty( $params[ 'filenameCallToAction' ] )) {
        $this->CALL_TO_ACTION_TRANSLATION_FILE = (string) $params[ 'filenameCallToAction' ];
      }
      if ( !empty( $params[ 'filenameCallToActionCaption' ] )) {
        $this->CALL_TO_ACTION_CAPTION_TRANSLATION_FILE = (string) $params[ 'filenameCallToActionCaption' ];
      }
      if ( !empty( $params[ 'filenameDefaultVCardTranslation' ] )) {
        $this->DEFAULT_VCARD_TRANSLATION_FILE = (string) $params[ 'filenameDefaultVCardTranslation' ];
      }
      if ( !empty( $params[ 'strDefaultPageDescription' ] ) ) {
        $this->DEFAULT_PAGE_DESCRIPTION = (string) $params[ 'strDefaultPageDescription' ];
      }
    }

    $this->id = $this->readPageIDFromQuery();
    $this->originalPageID = $this->id;
    $this->uriCanonical = $this->generateCanonicalURI();
    $this->exists = $this->doesPageExist();

    if ( !$this->exists ) {
      $this->dateLastModified = date( 'Y-m-d H:i', getlastmod());
      $this->title = $this->read404Title();
      $this->description = $this->readFriendly404Message();
      $this->id = "404";
    } else {
      $this->dateLastModified = date( 'Y-m-d H:i', filectime( "./$this->id/body" ));
      $this->title = $this->readTitle();
      $this->description = $this->readDescription();
      $this->ogImagePath = $this->readOgImage();
      $this->pinImagePath = $this->readPinImage();
      $this->twImagePath = $this->readTwImage();
    }
  }

  public function addCallToAction () {
    $text = $this->readTranslation( $this->CALL_TO_ACTION_TRANSLATION_FILE );
    $mustShow = ( !empty( $text) );
    if ( $mustShow ) {
      $caption = $this->readTranslation( $this->CALL_TO_ACTION_CAPTION_TRANSLATION_FILE );
      echo '<div class="body callToAction">';
      if ( !empty( $caption ) ) {
        $caption = "\n<h2 class=callToActionCaption>$caption</h2>\n";
      }
      echo $caption
            , "  <p class=callToActionText>"
            , $text
            , "</p>\n</div>\n";
    }
    unset($path, $text, $mustShow);
  }
  public function addExtendedCss ( $deeperFolder = '' ) {
    if ( is_string( $deeperFolder ) && !empty( $deeperFolder ) ) {
      $path = "./$this->originalPageID/$deeperFolder/xtend.css";
    } else {
      $path = "./$this->originalPageID/xtend.css";
    }
    $css = $this->readPart( $path );
    $mustShow = ( !empty( $css ) );
    if ( $mustShow ) {
      echo "<style>/* <![CDATA[ */\n"
            , $css
            , "\n/* ]]> */</style>\n";
    }
    unset($path, $css, $mustShow);
  }
  public function addExtendedHtml ( $deeperFolder = '' ) {
    if ( is_string( $deeperFolder ) && !empty( $deeperFolder ) ) {
      $path = "./$this->originalPageID/$deeperFolder/xtend.htmls";
    } else {
      $path = "./$this->originalPageID/xtend.htmls";
    }
    $htmls = $this->readPart( $path );
    $mustShow = ( !empty( $htmls ) );
    if ( $mustShow ) {
      echo '<div id="xtend">'
            , $htmls
            , "</div>\n";
    }
    unset($path, $htmls, $mustShow);
  }
  public function addExtendedJs ( $deeperFolder = '' ) {
    if ( is_string( $deeperFolder ) && !empty( $deeperFolder ) ) {
      $path = "./$this->originalPageID/$deeperFolder/xtend.js";
    } else {
      $path = "./$this->originalPageID/xtend.js";
    }
    $js = $this->readPart( $path );
    $mustShow = ( !empty( $js ) );
    if ( $mustShow ) {
      echo "<script>/* <![CDATA[ */\n"
            , $js
            , "\n/* ]]> */</script>\n";
    }
    unset($path, $js, $mustShow);
  }
  public function addNavMenu () {
    $i = 0;
    $link = '';
    $links = array();
    $path = '';
    $paths = array(
      "./$this->DEFAULT_PAGE_ID/navMenuForChildren",
      "./$this->DEFAULT_PAGE_ID/$this->langReq/navMenuForChildren",
      "./$this->originalPageID/../navMenuForChildren",
      "./$this->originalPageID/../$this->langReq/navMenuForChildren",
      "./$this->originalPageID/navMenu",
      "./$this->id/navMenu"
    );
    $i = count( $paths );
    while ( $i ) {
      $i -= 1;
      $path = $paths[ $i ];
      if ( file_exists( $path ) && is_file( $path ) ) {
        $i = 0;
      }
    }
    unset( $paths, $i );
    $links = $this->readPart( $path, true );
    if ( is_array( $links ) && !empty( $links ) ) {
      echo '<ul>';
      foreach( $links as $link ) {
        $pair = explode( '" <', substr( trim( $link ), 1, -1 ), 2 );
        if ( is_array( $pair ) && !empty( $pair )  && ( count( $pair ) == 2 ) ) {
          echo '<li class="'
                , preg_replace( '/[^-_a-zA-Z0-9]/', '', $pair[ 1 ] )
                , '"><a href="'
                , $pair[ 1 ]
                , '">'
                , $pair[ 0 ]
                , "</a></li>\n";
        }
      }
      echo "</ul>\n";
      unset( $pair, $link, $links );
    }
  }
  public function addPageBody () {
    $text = $this->readBody();
    $mustShow = ( !empty( $text) );
    if ( $mustShow ) {
      echo "<div class=body>$text</div>\n";
    }
    unset($text, $mustShow);
  }
  public function addPageComments () {
    if ( !file_exists( "./$this->originalPageID/noPageComments" )) {
      $this->addTranslation( "pagecomments" );
    }
  }
  public function addPageLinks ( $extraLIs = '' ) {
    if ( !file_exists( "./$this->originalPageID/noPageLinks" )) {
      $arrLinks = $this->readPart( "./$this->id/links", true );
      $hasPageLinks = ( !empty( $arrLinks ) );
      $mustShow = ( !empty( $extraLIs ) || $hasPageLinks ) ;
      $arrOutput = array();
      if ( $mustShow ) {
        echo "<h2 class=related>"
            , $this->readMoreLinksCaption()
            , ":</h2>\r<ul class=related itemscope itemtype='http://schema.org/ItemList'>";
        if ( $hasPageLinks ) {
          foreach( $arrLinks as $strLink ) {
            $arrLink = explode( '" <', substr( trim( $strLink ), 1, -1 ), 2 );
            if ( is_array( $arrLink ) && !empty( $arrLink) && ( count( $arrLink ) == 2 ) ) {
              if( strpos( $strLink, '://' ) === false ) {
                $nofollow = '' ;
              } else {
                $nofollow = " rel=nofollow ";
              }
              $arrOutput[] = '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item relatedItem" itemscope itemtype="http://schema.org/Article"><a itemprop="url"' . $nofollow . ' href="' . $arrLink[ 1 ] . '"><span itemprop="name">' . $arrLink[ 0 ] . "</span></a></span></li>";
            }
          }
        }
        echo implode("\n", $arrOutput)
            , $extraLIs
            , "\n</ul>\n";
      }
      unset($arrLinks, $arrOutput, $hasPageLinks, $strLink, $nofollow, $mustShow);
    }
  }
  public function addSearchForm () {
    $this->addTranslation( "searchform" );
  }
  public function addSocialSharingButtons () {
    if ( !file_exists( "./$this->originalPageID/noSocialSharing" )) {
      $url = $this->CANONICAL_SECTION_URI;
      /*
      * The social-sharing include needs the variables $queryArticlePath, $queryArticleTitle, and $queryArticlePinMedia.
      * It provides the domain name with forward slash.
      */
      if ( $this->originalPageID !== $this->DEFAULT_PAGE_ID ) {
        $queryArticlePath = $url . "?id=$this->originalPageID";
      } else {
        $queryArticlePath = $url;
      }
      $queryArticleTitle = $this->title;
      if ( $this->originalPageID !== $this->DEFAULT_PAGE_ID ) {
        $queryArticleTitle .= " - " . $this->DEFAULT_PAGE_DESCRIPTION;
      }
      $queryArticlePinMedia = $this->pinImagePath;
      include( "$this->FILE_PATH_TO_TRANSLATIONS/socialsharing" );
      unset($url, $queryArticlePath, $queryArticleTitle, $queryArticlePinMedia);
    }
  }
  public function addTranslation ( $which = '' ) {
    echo $this->readTranslation( $which );
  }
  public function addVCard () {
    $vcard = $this->readTranslation( $this->readPart( "./$this->id/vcard" ) );
    if ( empty( $vcard ) ) {
      $vcard = $this->readTranslation( $this->readPart( "./$this->originalPageID/vcard" ) );
    }
    if ( empty( $vcard ) ) {
      $vcard = $this->readTranslation( $this->readPart( "./vcard" ) );
    }
    if ( empty( $vcard ) ) {
      $vcard = $this->readTranslation( $this->DEFAULT_VCARD_TRANSLATION_FILE );
    }
    echo $vcard;
  }
  public function addWarningDeprecatedBrowser () {
    $this->addTranslation( "warningDeprecatedBrowser" );
  }
  public function doesPageExist () {
    $pathToBody = "./$this->id/body";
    $it = false;
    if ( file_exists( $pathToBody ) && is_file( $pathToBody ) ) {
      $it = true;
    }
    unset($pathToBody);
    return $it;
  }
  public function generateBodyClass () {
    return ' class="' . preg_replace('/[^-_a-zA-Z0-9]/', '', $this->originalPageID) . '"';
  }
  public function generateCanonicalURI () {
    if ( $this->originalPageID !== $this->DEFAULT_PAGE_ID ) {
      return $this->CANONICAL_SECTION_URI . "?id=$this->originalPageID";
    }
    return $this->CANONICAL_SECTION_URI;
  }
  protected function lastExistingFileFromArray ( array $paths = array() ) {
    $result = '';
    $i = count( $paths );
    while ( $i ) {
      $i -= 1;
      $path = $this->readPart( $paths[ $i ] );
      $path = strip_tags( $this->parseNoCode ( $path ) );
      if ( !empty( $path ) ) {
        $result = $path;
        $i = 0;
      }
    }
    unset($i, $path);
    return $result;
  }
  protected function parseBBCode ( $strInput ) {
    return bb2html( htmlspecialchars( $strInput ));
  }
  protected function parseNoCode ( $strInput ) {
    return htmlspecialchars( strip_tags( $strInput ));
  }
  protected function read404Title () {
    return $this->parseNoCode( $this->readTranslation( "404title", "404 Not Found" ));
  }
  protected function readBody () {
    return $this->parseBBCode( $this->readPart( "./$this->id/body" ));
  }
  public function readContentType () {
    //OpenGraph: [article, activity, author, blog, book, cause, city, company, game, music.song, product, profile, public_figure, website, etc.]
    return strip_tags( $this->parseNoCode($this->readPart( "./$this->originalPageID/ogType", false, "article" )));
  }
  protected function readDescription () {
    return $this->substituteQuotes( strip_tags( $this->parseNoCode($this->readPart( "./$this->id/description", false, $this->DEFAULT_PAGE_DESCRIPTION ))));
  }
  protected function readFriendly404Message () {
    return $this->readTranslation( "404friendlyMessage", "Does that address exist?" );
  }
  public function readIntro () {
    return nl2br( $this->parseNoCode( $this->readPart( "./$this->id/intro" )));
  }
  public function readLastModifiedCaption () {
    return $this->readTranslation( "lastModified", "Last Modified: " );
  }
  protected function readMoreLinksCaption () {
    return $this->readTranslation( "morelinks", "Explore" );
  }
  protected function readOgImage () {
    $result = $this->lastExistingFileFromArray(
      array(
        "./ogImage",
        "./$this->originalPageID/ogImage",
        "./$this->id/ogImage"
      )
    );
    if ( empty( $result ) ) {
      $result = '';
    }
    return $result;
  }
  protected function readPageIDFromQuery () {
    $idTemporary = '';
    if ( empty( $_GET[ "id" ] )) {
      return $this->DEFAULT_PAGE_ID;
    }
    $idTemporary = htmlspecialchars( $_GET[ "id" ] );
    if ( empty( $idTemporary )) {
      return $this->DEFAULT_PAGE_ID;
    }
    return $idTemporary;
  }
  protected function readPart ( $strPartname, $bArray = false, $strDefault = '' ) {
   if ( $bArray ) {
    return ( ( file_exists( $strPartname ) && is_file($strPartname) ) ? file( $strPartname ) : Array());
   } else {
    return ( file_exists( $strPartname ) && is_file($strPartname) ) ? file_get_contents( $strPartname ) : $strDefault;
   }
  }
  protected function readPinImage () {
    $result = $this->lastExistingFileFromArray(
      array(
        "./pinImage",
        "./$this->originalPageID/pinImage",
        "./$this->id/pinImage"
      )
    );
    if ( empty( $result ) ) {
      $result = $this->readOgImage();
    }
    return $result;
  }
  public function readSchemaItemType () {
    //Schema.org: [Article, Product, Movie, Review, MusicComposition, etc. ]
    return strip_tags($this->parseNoCode($this->readPart( "./$this->originalPageID/schemaItemType", false, "Article" )));
  }
  protected function readTitle () {
    return $this->substituteQuotes( $this->parseNoCode( $this->readPart( "./$this->id/title", false, "No Title" )));
  }
  public function readTranslation ( $which = '', $default = '' ) {
    $result = '';
    if ( !empty( $which ) ) {
      $path = "$this->FILE_PATH_TO_TRANSLATIONS/$which";
      $result = $this->readPart( $path, false, $default );
      unset($path);
    }
    return $result;
  }
  public function readTwitterCardType () {
    //[app, gallery, player, photo, summary, video, etc.]
    return strip_tags($this->parseNoCode($this->readPart( "./$this->originalPageID/twCardType", false, "summary" )));
  }
  protected function readTwImage () {
    $result = $this->lastExistingFileFromArray(
      array(
        "./twImage",
        "./$this->originalPageID/twImage",
        "./$this->id/twImage"
      )
    );
    if ( empty( $result ) ) {
      $result = '';
    }
    return $result;
  }
  protected function substituteQuotes ( $strInput = '' ) {
    $result = $strInput;
    if ( !empty( $strInput) ) {
      $result = str_replace('"', '&rdquo;', $result);
      $result = str_replace("'", '&rsquo;', $result);
    }
    return $result;
  }
} // end class PublizjrPage
