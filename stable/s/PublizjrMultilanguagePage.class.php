<?php
/** Multilingual Publizjr Web Page Generator Class.
*
* Assembles article parts into html web pages.
* @link https://omegajunior.globat.com/code/publizjr/?id=publizjr-2-api-doc
* @author A.E.Veltstra <publizjr@protonmail.com>
* @copyright Copyright (c) 1993 - 2016, A.E.Veltstra for OmegaJunior.Net
* @version 20160808t1130
*
* @package Publizjr
*/
/**
* Extended class. 
* 
* Expects file PublizjrPage.class.php to live in same directory.
*/
require_once dirname(__FILE__) . '/PublizjrPage.class.php';
/**
* To recognise the language requested by the user agent.
* 
* Contains the function zjrAcceptLanguage(). Expects file detectlang.php to live in same directory.
*/
require_once dirname(__FILE__) . '/detectlang.php';
/** 
* Class PublizjrMultilanguagePage
*
* Sample usage: 
* require_once dirname(__FILE__) . '/PublizjrMultilanguagePage.class.php';
* $page = new PublizjrMultilanguagePage( $params );
*
* @param array $params 
* @see __construct()
*/
final class PublizjrMultilanguagePage extends PublizjrPage {

  /** 
  * PublizjrMultilanguagePage Class Constructor
  * 
  * Sample usage: 
  *
  * define( 'DEFAULT_PAGE_ID', 'index' );
  * define( 'DEFAULT_PAGE_DESCRIPTION', '&Omega;Jr. Publizjr - Creates Web Pages' );
  * define( 'CANONICAL_SECTION_URI', 'https://omegajunior.globat.com/code/publizjr/' );
  *
  * $params = array(
  *  'defaultPageID'                   => DEFAULT_PAGE_ID,
  *  'uriCanonicalSection'             => CANONICAL_SECTION_URI, 
  *  'filePathToTranslations'          => './s/translations',
  *  'filenameCallToAction'            => 'callToAction',
  *  'filenameCallToActionCaption'     => 'callToActionCaption', 
  *  'filenameDefaultVCardTranslation' => 'vCard',
  *  'strDefaultPageDescription'       => DEFAULT_PAGE_DESCRIPTION
  * );
  *
  * $page = new PublizjrMultilanguagePage( $params );
  *
  * @param array $params
  */
  public function __construct ( array $params = array() ) {
    parent::__construct( $params );
    $this->langReq = zjrAcceptLanguage();
    $this->FILE_PATH_TO_TRANSLATIONS .= '/' . $this->langReq;
    $this->id = $this->adjustPageIDForRequestedLanguage();
    $this->exists = $this->doesPageExist();
    if ( $this->exists ) {
      $this->dateLastModified = date( 'Y-m-d H:i', filectime( "./$this->id/body" ) );
      $this->title = $this->readTitle();
      $this->description = $this->readDescription();
      $this->ogImagePath = $this->readOgImage();
      $this->pinImagePath = $this->readPinImage();
      $this->twImagePath = $this->readTwImage();
    }
  }
  private function adjustPageIDForRequestedLanguage () {
    if ( empty( $this->langReq ) ) {
      return $this->originalPageID;
    }
    if ( file_exists( $this->originalPageID . '/' . $this->langReq ) && is_dir( $this->originalPageID . '/' . $this->langReq ) ) {
      return $this->originalPageID . '/' . $this->langReq;
    }
    return $this->originalPageID;
  }
} // end class PublizjrMultilanguagePage