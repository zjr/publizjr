Publizjr 2 - Creates Web Pages
===========================================================
A.E.Veltstra for OmegaJunior.Net
https://omegajunior.globat.com/code/publizjr/
Release 2.20160616t0830
-----------------------------------------------------------
1. Package Contents;
2. Installation Instructions; 
3. Configuration Instructions;

===========================================================
1. Package Contents
-----------------------------------------------------------
- browserconfig.xml
- manifest.json
- ogImage
- pinImage
- publizjr.php
- readme.txt
- twImage
+ c/
- - publizjr.20150602t1130.css
- i/
- - (many images)
+ s/
- - bb2html.php
- - detectlang.php
- - PublizjrMultilanguagePage.class.php
- - PublizjrPage.abstract.php
- - PublizjrPage.class.php
- - PublizjrPage.interface.php
- + translations/
- - - (many text files) 
- - + en/
- - - - (many text files) 
- - + nl/
- - - - (many text files) 

===========================================================
2. Installation Instructions
-----------------------------------------------------------
Prerequisites

* Access to a functioning web server that runs PHP5;
* Programming experience with PHP5;
* Knowledge about uploading and creating files and folders on the web server;
* Knowledge about setting access privileges;
* Knowledge about configuring Default Documents for web servers.


Installation Process:

1. Download Required Parts - you did that otherwise you can't read this;
2. Unzip Parts - you did that;
3. Copy everything to a test folder on your webserver.

===========================================================
3. Configuration Instructions
-----------------------------------------------------------
Steps:

3.1. Configure Template - the publizjr.php;
3.2. Configure Translations - in the s/translations folder;
3.3. Configure Default Document;
3.4. Configure Access Privileges - on the folders of your webserver's file system;
3.5. Test;
3.6. Create Content.

-----------------------------------------------------------
3.1. Configure Template

By default, everything will work, and will contain (and thus output) undesirable texts.

Open publizjr.php in your favourite PHP text editor and read through it. Where configuation applies, either comments will indicate to customize, or it should be apparent from the contents.

You may wish to insert different HTML, depending on your site's needs. Additionally, you may wish to customize the default CSS.

If you need multilanguage awareness, switch the $page's class from PublizjrPage to PublizjrMultilanguagePage. The class should take care of everything except 2 changes you need to make:

- Set the 'lang' attribute of the HTML element to the output of $page->langReq;

- Set the contstructor parameter 'filePathToTranslations' to the translations folder itself, rather than a locale-specific child folder. The class will then choose the correct localisation.


Additionally, this package contains a few files that require configuration, too:

- browserconfig.xml
- manifest.json
- ogImage
- pinImage
- twImage

Each of these specify image locations. Each is used by a different computing platform or social medium. Edit each file and make sure all image addresses point to an absolute web address.


A quick run-down:

- browserconfig.xml is read by Microsoft Windows 8 to figure out how to style a dashboard tile if the visitor chooses to bookmark the web page. 

- manifest.json is read by Firefox and similar browsers to figure out how to style web page bookmarks. 

- ogImage is read by Publizjr and used to tell Facebook which image to show when sharing the web page.

- pinImage is read by Publizjr and used to tell Pinterest which image to show when sharing the web page.

- twImage is read by Publizjr and used to tell Twitter which image to show when sharing the web page.

-----------------------------------------------------------
3.2. Configure Translations

This package includes a folder named s/translations/. It contains bits of text, html, PHP, etc., that should be localised into the linguistic culture your site wishes to support.

We have provided 2 locales: English (en) and Dutch (nl). 

The localised bits live in their locale-specific folder inside the translations folder:

+ s/
- + translations/
- - + en/
- - + nl/

In this package, you will find the same translation bits in the folders 's/translations/' and 's/translations/en/'. That's because English is the default locale.

Go through them and see if there's anything you would like to change. Specifically, pay attention to these translations:

- favicons (currently points to the Publizjr logos in the 'i' folder);
- pagecomments (currently uses OmegaJunior's Disqus account);
- searchform (currently points to a nonexisting page '?id=found');
- socialsharing (currently points each share back to the omegajunior.net).

If you wish to add a locale, create a new folder, assign the correct 2 or 3-letter language abbreviation code, drop in a copy of the default translations, and start translating them.

Publizjr uses a mechanism to figure out which language the page visitor likes to read. This mechanism looks at the language preference in their web browser. We discuss it here:
http://www.omegajunior.net/code/?id=htmlacceptlanguage

-----------------------------------------------------------
3.3. Configure Default Document

As long as you are customizing and testing, continue to use your existing home page. Once you feel happy with your results, and you decided Publizjr should generate your homepage, it is wise to configure the web server to use Publizjr as its Default Document.

In .htaccess files, this can be done using the following instruction:
DirectoryIndex publizjr.php

If your webserver / host does not allow editing of .htaccess files, try renaming the publizjr.php into 'index.php'.

-----------------------------------------------------------
3.4. Configure Access Privileges

Since each page is generated out of text files in a page folder, you can and should configure access privileges. Each content author can receive access grants inside their page folders.

Since the folders live on the webserver's file system, you can use your favourite method for assigning grants. 

Most web hosts that allow direct FTP access and account creation, also allow grant assignments. Some allow SSH access. The host for the OmegaJunior.Net, Globat Web Hosting, offers a graphical user interface as a web application, to allow grants assignments. 

Use whichever works best for you.

After having their accounts created and been granted access privileges, each content author can use their favourite FTP client to change their pages. Simply overwriting any text file should change the page contents.

-----------------------------------------------------------
3.5. Test

As long as you are in the process of (re)configuring, you may want to create a copy of the Publizjr Generator template using a different name.

- Check that the template can read the bb2html.php file and the contents of the "translations" folder. Recognition is easy: the web page will stop working. If you uncomment the PHP error instructions on lines 12 and 13, you can see the error message.

- Make sure the template can read the "index" page folder, for by default this will contain the home page.

- See to it that the template shows the contents of the index page folder if no page id is provided in the web address.

- Test whether the template shows a 404 message when you provide a page id for which no folder exists, or when the page folder does not contain a "body" file.

-----------------------------------------------------------
3.6. Create Content

Read the instructions here:
https://omegajunior.globat.com/code/publizjr/?id=quickstart

===========================================================